package com.ttxz.versionplugin;

/**
 * 类作用的简要说明（必填）<br />
 * 类作用的详细说明（选填）
 *
 * @author xieshiqiang
 * @email xieshiqiang@newbeiyang.com
 * @date 2021/11/3 14:02
 */
public interface DependencyManager {

    interface BuildVersion {
        int compileSdkVersion = 29;
        String buildToolsVersion = "29.0.2";
        int minSdkVersion = 17;
        int targetSdkVersion = 26;
        int versionCode = 102;
        String versionName = "1.0.0";
    }

    /**
     * 项目相关配置
     */
    interface BuildConfig {
        //AndroidX
        String appcompat = "androidx.appcompat:appcompat:1.2.0";
        String constraintLayout = "androidx.constraintlayout:constraintlayout:2.1.1";
        String coreKtx = "androidx.core:core-ktx:1.3.2";
        String material = "com.google.android.material:material:1.4.0";
        String junittest = "androidx.test.ext:junit:1.1.3";
        String swiperefreshlayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0";
        String recyclerview = "androidx.recyclerview:recyclerview:1.1.0";
        String cardview = "androidx.cardview:cardview:1.0.0";

        //Depend
        String junit = "junit:junit:4.12";
        String espresso_core = "androidx.test.espresso:espresso-core:3.3.0";
        String guava = "com.google.guava:guava:24.1-jre";
        String commons = "org.apache.commons:commons-lang3:3.6";
        String zxing = "com.google.zxing:core:3.3.2";

        //leakcanary
        String leakcanary = "com.squareup.leakcanary:leakcanary-android:2.4";

        //jetPack
        String room_runtime = "androidx.room:room-runtime:2.2.5";
        String room_compiler = "androidx.room:room-compiler:2.2.5";
        String room_rxjava2 = "androidx.room:room-rxjava2:2.2.5";
        String lifecycle_extensions = "android.arch.lifecycle:extensions:1.1.1";
        String lifecycle_compiler = "android.arch.lifecycle:compiler:1.1.1";
        String rxlifecycle = "com.trello.rxlifecycle3:rxlifecycle:3.1.0";
        String rxlifecycle_components = "com.trello.rxlifecycle3:rxlifecycle-components:3.1.0";
    }

}
